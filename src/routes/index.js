import Home from "@pages/Home";
// import Settings from "@pages/Settings";
import { appElement } from "@config/app";

const contentSection = appElement;

export class Router {
  constructor() {
    this.renderRoute();
    history.pushState({}, "", "/");

    // const dom = document.querySelector('[js-attr="app"]');
  }

  getRoutes() {
    return [
      {
        path: "/",
        match: ["/"],
        page: Home
      }
      // {
      //   path: "/settings",
      //   page: Settings
      // }
    ];
  }
  renderRoute() {
    //Check if route exist
    const findRoute = this.getRoutes().find(
      route => route.path === window.location.pathname
    );
    if (findRoute && findRoute.page) {
      //Finding the right route
      //Render it
      contentSection.innerHTML = findRoute.page.render();
      //Run mounted hook
      findRoute.page.mounted();
      return true;
    } else {
      console.log("no route");
      // window.location.href = "/";
      history.pushState({}, "", "/");
      this.update();
      return true;
    }
  }

  get currentRoute() {
    return this.renderRoute();
  }
  changeRoute() {
    window.location.href = window.location.pathname;
  }
  update() {
    this.renderRoute();
  }
}
export const router = new Router();

export default router;
