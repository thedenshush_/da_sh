import { renderTemplate } from "@utils/template";
import template from "./index.html";
import itemTemplate from "./item.html";
import HomePage from "@pages/Home";
import SettingsPage from "@pages/Settings";
import Router from "@routes/";
import "./style.scss";

class AboutPage {
  constructor() {
    this.items = [
      {
        name: "Name",
        value: "John Doe",
        icon: "profile"
      },
      {
        name: "Web Page",
        value: "seller.com",
        icon: "webpage"
      },
      {
        name: "phone",
        icon: "phone",
        value: "(123) 232-1233"
      },
      {
        name: "address",
        icon: "address",
        value: "Some Address"
      }
    ];
    this.state = {
      elementName: "about-page",
      anchor: "clickButton",
      name: "about",
      title: "ABOUT",
      items: this.items
        .map(item => renderTemplate(itemTemplate, { ...item }))
        .join("")
    };
    this.swipe = {
      startX: 0,
      endX: 0
    };

    this.elementName = this.state.elementName;
    this.node = null;
    this.anchor = this.state.anchor;
  }

  mounted() {
    if (
      window.location.pathname === "/about" ||
      window.location.pathname === "/"
    ) {
      this.node = document.querySelector(`[js-attr="${this.elementName}"]`);
      this.node.addEventListener("touchstart", event => {
        this.swipe.startX = event.touches[0].clientX;
      });
      this.node.addEventListener("touchend", event => {
        this.swipe.endX = event.changedTouches[0].clientX;
        this.events.handleTouch();
      });

      // Handle modals
      this.node
        .getElementsByClassName("about-list")[0]
        .querySelectorAll('[js-attr="about-item"]')
        .forEach(item => {
          //Open Modal
          item
            .querySelector('[js-attr="item-click"]')
            .addEventListener("click", () => {
              this.events.openModal(item);
            });

          //Close Modal button
          item
            .querySelector('[js-attr="modal-close"]')
            .addEventListener("click", () => {
              this.events.closeModals(item);
            });

          item
            .querySelector('[js-attr="modal-save"]')
            .addEventListener("click", () => {
              const value = item.querySelector('[js-attr="input"]').value;
              const node = item
                .querySelector('[js-attr="input"]')
                .getAttribute("js-attr-name");
              this.events.updateValue({ value, node });
            });
        });
    }
  }

  events = {
    click: () => {},
    handleTouch: () => {
      if (this.swipe.startX - this.swipe.endX > 100) {
        HomePage.goTo("settings");
        history.pushState({}, "", `settings`);
        SettingsPage.mounted();
      }
    },
    openModal(item) {
      this.closeModals(item);
      item.querySelector('[js-attr="modal"]').classList.add("active");
    },
    closeModals(item) {
      const modals = item.parentElement.querySelectorAll('[js-attr="modal"]');
      if (modals) {
        modals.forEach(modal => modal.classList.remove("active"));
      }
    },
    updateValue: ({ value, node }) => {
      // this.
      const correct = this.items.find(item => item.name === node);
      correct.value = value;
      HomePage.update();
      Router.update();
    }
  };
  render() {
    return renderTemplate(template, this.state);
  }
}

const aboutPage = new AboutPage();
export default aboutPage;
