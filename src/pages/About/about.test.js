import Component from "./index";

jest.mock("@utils/template", () => ({
  renderTemplate: jest.fn(() => ["asd", "asd"])
}));
jest.mock("@routes/", () => ({
  upadate: jest.fn()
}));

test("testing about", () => {
  const template = `<section js-attr="about-page" class="about-page">
  <h1>{{title}}</h1>

  <div js-attr="about-list" class="about-list">
  <div class="about-list__item item" js-attr="about-item">
  <div class="item__icon item__icon-{{icon}}"></div>
  <div class="item__text">{{value}}</div>
  <div js-attr="item-click" class="item__changeicon"></div>
  <div>
        <div js-attr="modal" class="modal ">
        <div class="group">
            <input
            js-attr="input"
            js-attr-name="{{name}}"
            value="asdasd"
            type="text"
            required
            />
            <span class="highlight"></span>
            <span class="bar"></span>
            <label>{{name}}</label>
        </div>
        <div class="modal-buttons">
            <button
            js-attr="modal-save"
            class="material-button material-button-success"
            >
            Save
            </button>
            <button
            js-attr="modal-close"
            class="material-button material-button-danger"
            >
            Close
            </button>
        </div>
        </div>
        </div>
    </div>
  </div>
</section>
`;

  const templateItem = `<div class="about-list__item item" js-attr="about-item">
<div class="item__icon item__icon-{{icon}}"></div>
<div class="item__text">{{value}}</div>
<div js-attr="item-click" class="item__changeicon"></div>
<div>
  <div js-attr="modal" class="modal ">
    <div class="group">
      <input
        js-attr="input"
        js-attr-name="{{name}}"
        value="asdasd"
        type="text"
        required
      />
      <span class="highlight"></span>
      <span class="bar"></span>
      <label>{{name}}</label>
    </div>
    <div class="modal-buttons">
      <button
        js-attr="modal-save"
        class="material-button material-button-success"
      >
        Save
      </button>
      <button
        js-attr="modal-close"
        class="material-button material-button-danger"
      >
        Close
      </button>
    </div>
  </div>
</div>
</div>
`;
  const item = document.createElement("div");
  item.innerHTML = templateItem;
  document.body.innerHTML = template;
  history.pushState({}, "", `/`);

  document
    .getElementsByClassName("about-list")[0]
    .dispatchEvent(new Event("click"));
  document
    .querySelector('[js-attr="item-click"]')
    .dispatchEvent(new Event("click"));
  document
    .querySelector('[js-attr="modal-close"]')
    .dispatchEvent(new Event("click"));
  document
    .querySelector('[js-attr="modal-save"]')
    .dispatchEvent(new Event("click"));

  Component.render();
  Component.mounted();
  Component.events.openModal(item);
});
