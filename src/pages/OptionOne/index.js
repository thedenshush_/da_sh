import { renderTemplate } from "@utils/template";
import template from "./index.html";
// import Router from "@routes/";
import "./style.scss";
class OptionOneComponent {
  constructor() {
    this.state = {
      elementName: "option-one-component"
    };
    this.elementName = this.state.elementName;
    this.node = null;
  }

  mounted() {
    this.node = document.querySelector(`[js-attr="${this.elementName}"]`);
    // this.node
    //   .querySelector(`[js-attr="button"]`)
    //   .addEventListener("click", () => {
    //     this.events.click();
    //   });
  }
  events = {
    // click: () => {
    //   console.log("click", window.history);
    //   history.pushState({ path: "/settings" }, "homepage", "/settings");
    //   Router.update();
    // }
  };
  render() {
    return renderTemplate(template, this.state);
  }
}

const optionOnecomponent = new OptionOneComponent();
export default optionOnecomponent;
