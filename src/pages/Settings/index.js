import { renderTemplate } from "@utils/template";
import template from "./index.html";
import HomePage from "@pages/Home";
// import Router from "@routes/";
import "./style.scss";
class SettingsPage {
  constructor() {
    this.state = {
      name: "settings",
      elementName: "settingsPage"
    };
    this.elementName = this.state.elementName;
    this.node = null;
    this.swipe = {
      startX: 0,
      endX: 0
    };
  }

  mounted() {
    // history.pushState({}, "", "/settings");
    if (window.location.pathname === "/settings") {
      this.node = document.querySelector(`[js-attr="${this.elementName}"]`);

      this.node.addEventListener("touchstart", event => {
        this.swipe.startX = event.touches[0].clientX;
      });
      this.node.addEventListener("touchend", event => {
        this.swipe.endX = event.changedTouches[0].clientX;

        this.events.handleTouch();
      });
    }
  }
  events = {
    click: () => {},
    handleTouch: () => {
      if (this.swipe.endX > this.swipe.startX) {
        HomePage.goTo("settings");
        history.pushState({}, "", `about`);
      }
    }
  };
  render() {
    return renderTemplate(template, this.state);
  }
}

const settingsPage = new SettingsPage();
export default settingsPage;
