import { renderTemplate } from "@utils/template";
import template from "./index.html";
import Router from "@routes/";
import "./style.scss";
import AboutComponent from "@pages/About";
import SettingsComponent from "@pages/Settings";
import ProfileComponent from "@components/Profile";
import NavBarComponent from "@components/NavBar";

class HomePage {
  constructor() {
    this.state = {
      name: "home",
      elementName: "homePage",
      childrenComponent: null,
      navButton: "navButton",
      logoutText: "LOG OUT",
      ActivePage: null,
      ProfileComponent: ProfileComponent.render(),
      NavBarComponent: NavBarComponent.render()
    };
    this.template = renderTemplate(template, this.state);
    this.subpages = [AboutComponent, SettingsComponent];
    this.elementName = this.state.elementName;
    this.node = null;
    this.setActivePage(this.subpages[0]);
  }
  setActivePage(page) {
    this.state.ActivePage = page.render();
  }
  mounted() {
    this.node = document.querySelector(`[js-attr="${this.elementName}"]`);
    this.subpages.forEach(async page => {
      await page.render();
      page.mounted();
      NavBarComponent.mounted();
    });
    ProfileComponent.mounted();
    this.node
      .querySelectorAll(`[js-attr="${this.state.navButton}"]`)
      .forEach(element => {
        element.addEventListener("click", event => {
          this.goTo(event.target.attributes["js-path"].value);
        });
      });
  }
  goTo(path) {
    this.setActivePage(this.subpages.find(page => page.state.name === path));

    Router.update();
  }

  update() {
    this.state.ProfileComponent = ProfileComponent.render();
  }
  render() {
    return renderTemplate(template, this.state);
  }
}

const homePage = new HomePage();
export default homePage;
