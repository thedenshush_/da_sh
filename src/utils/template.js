export const renderTemplate = (template, payload) => {
  var re = /{{([^}}]+)?}}/g,
    match;
  while ((match = re.exec(template))) {
    template = template.replace(match[0], payload[match[1]]);
  }
  return template;
};
