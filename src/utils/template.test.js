import { renderTemplate } from "./template";

test("testing AboutComponent", () => {
  const template = `
  <h1>{{test}}</h1>
  `;
  const state = {
    test: "test"
  };
  const renderedTemplate = renderTemplate(template, state);
  expect(renderedTemplate).toBe(`
  <h1>test</h1>
  `);
  // expect("s").toBe("s");
});
