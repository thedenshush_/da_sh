export const appElement = document.querySelector('[js-attr="app"]');

export const headerElement = document.querySelector('[js-attr="header"]');

export const contentElement = document.querySelector('[js-attr="content"]');
