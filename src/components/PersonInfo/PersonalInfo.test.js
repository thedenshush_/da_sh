import PersonalInfo from "./index";

jest.mock("@utils/template", () => ({
  renderTemplate: jest.fn(
    () =>
      `<div js-attr="{{elementName}}" class="person-info">
        <b class="person-info__name">Jessica Parker</b>
        <div class="person-info__description person-description">
          <span class="person-description__icon icon-address"></span>
          <span class="person-description__text">{{address}}</span>
        </div>
        <div class="person-info__description person-description">
          <span class="person-description__icon icon-call"> icon </span>
          <span class="person-description__text"> {{phone}} </span>
        </div>
      </div>
      `
  )
}));

test("testing PersonalInfo", () => {
  const template = `<div js-attr="{{elementName}}" class="person-info">
    <b class="person-info__name">Jessica Parker</b>
    <div class="person-info__description person-description">
      <span class="person-description__icon icon-address"></span>
      <span class="person-description__text">{{address}}</span>
    </div>
    <div class="person-info__description person-description">
      <span class="person-description__icon icon-call"> icon </span>
      <span class="person-description__text"> {{phone}} </span>
    </div>
  </div>
  `;
  document.body.innerHTML = template;
  PersonalInfo.render();
  PersonalInfo.mounted();
});
