import { renderTemplate } from "@utils/template";
import template from "./index.html";
import "./styles.scss";

class PersonInfoComponent {
  constructor() {
    this.state = {
      name: "personal-info",
      personName: "Jessica Parker",
      elementName: "personal-info",
      address: "Newport Beach, CA",
      phone: "(949) 325-68594",
      items: "asdasd"
    };
    this.elementName = this.state.elementName;
    this.node = null;
    this.state.items = this.items = this.state.items;
  }

  mounted() {
    this.node = document.querySelector(`[js-attr="${this.elementName}"]`);
  }
  events = {
    click: () => {}
  };
  render() {
    return renderTemplate(template, this.state);
  }
}

const personInfoComponent = new PersonInfoComponent();
export default personInfoComponent;
