import Profile from "./index";
import "../../../__mocks__/watchMediaMock";

jest.mock("@utils/template", () => ({
  renderTemplate: jest.fn()
}));
jest.mock("@components/PersonInfo", () => ({
  render: jest.fn(),
  mounted: jest.fn()
}));
jest.mock("@components/StarsComponent", () => ({
  render: jest.fn(),
  mounted: jest.fn()
}));
jest.mock("@components/Followers", () => ({
  render: jest.fn(),
  mounted: jest.fn()
}));
test("testing Profile", () => {
  const template = `<div js-attr="{{elementName}}" class="profile-component">
    <div class="profile-component__profile-info">
        <img
        src="../../assets/profile.jpg"
        class="profile-component__image"
        alt=""
        />
        {{PersonalInfo}}
    </div>
    <div class="profile-component__stars">
        {{StarsComponent}}
    </div>
</div>`;
  document.body.innerHTML = template;
  // Profile.render();
  Profile.mounted();
});
