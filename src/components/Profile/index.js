import { renderTemplate } from "@utils/template";
import template from "./index.html";
import PersonalInfo from "@components/PersonInfo";
import StartComponent from "@components/StarsComponent";
import FollowersComponent from "@components/Followers";

import "./styles.scss";

class ProfileComponent {
  constructor() {
    this.state = {
      name: "profile-component",
      elementName: "profile-component",
      PersonalInfo: PersonalInfo.render(),
      StarsComponent: StartComponent.render(),
      FollowersComponent: FollowersComponent.render()
    };
    this.elementName = this.state.elementName;
    this.node = null;
  }

  mounted() {
    this.node = document.querySelector(`[js-attr="${this.elementName}"]`);
    const mobileSize = window.matchMedia("(max-width: 940px)");
    PersonalInfo.mounted();
    StartComponent.mounted();
    FollowersComponent.mounted();
    if (window.screen.width > 940) {
      this.node
        .querySelector(`[js-attr="${FollowersComponent.state.elementName}"]`)
        .classList.add("navbar-section__followers-hide");
    }

    mobileSize.addEventListener("change", media => {
      if (!media.matches) {
        this.node
          .querySelector(`[js-attr="${FollowersComponent.state.elementName}"]`)
          .classList.add("navbar-section__followers-hide");
      } else {
        this.node
          .querySelector(`[js-attr="${FollowersComponent.state.elementName}"]`)
          .classList.remove("navbar-section__followers-hide");
      }
    });
  }
  events = {
    click: () => {}
  };
  render() {
    return renderTemplate(template, this.state);
  }
}

const profileComponent = new ProfileComponent();
export default profileComponent;
