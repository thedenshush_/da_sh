import Followers from "./index";
jest.mock("@utils/template", () => ({
  renderTemplate: jest.fn(
    () =>
      `<div js-attr="followersComponent" class="navbar-section__followers followers">
        <div class="followers__plus followers-plus"></div>
        <div class="followers__text">15 followers</div>
      </div>
      `
  )
}));

test("Testing Followers component", () => {
  const template = `<div js-attr="followersComponent" class="navbar-section__followers followers">
    <div class="followers__plus followers-plus"></div>
    <div class="followers__text">15 followers</div>
  </div>
  `;
  Followers.render();
  Followers.mounted();
});
