import { renderTemplate } from "@utils/template";
import template from "./index.html";
import "./styles.scss";

export class FollowersComponent {
  constructor() {
    this.state = {
      name: "followers",
      elementName: "followersComponent",
      followersNumber: "15 followers"
    };
    this.elementName = this.state.elementName;
    this.node = null;
    this.state.items = this.items = this.state.items;
  }

  mounted() {
    this.node = document.querySelector(`[js-attr="${this.elementName}"]`);
  }
  events = {
    click: () => {}
  };
  render() {
    return renderTemplate(template, this.state);
  }
}

const component = new FollowersComponent();
export default component;
