import { renderTemplate } from "@utils/template";

import template from "./index.html";
import "./styles.scss";

class PersonInfoComponent {
  constructor() {
    this.state = {
      name: "stars-component",
      elementName: "stars-component"
    };
    this.elementName = this.state.elementName;
    this.node = null;
  }

  mounted() {
    this.node = document.querySelector(`[js-attr="${this.elementName}"]`);
  }
  events = {
    click: () => {}
  };
  render() {
    return renderTemplate(template, this.state);
  }
}

const personInfoComponent = new PersonInfoComponent();
export default personInfoComponent;
