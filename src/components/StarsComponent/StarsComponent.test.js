import Component from "./index";
jest.mock("@utils/template", () => ({
  renderTemplate: jest.fn(
    () =>
      `<div js-attr="stars-component" class="stars-section">
      <div class="stars-group">
        <div class="stars-group__star"></div>
        <div class="stars-group__star"></div>
        <div class="stars-group__star"></div>
        <div class="stars-group__star"></div>
        <div class="stars-group__star-half"></div>
      </div>
      <div class="stars-section__reviewrs">
        <span>6 rewiever</span>
      </div>
    </div>
      `
  )
}));

test("Testing Stars component", () => {
  Component.render();
  Component.mounted();
});
