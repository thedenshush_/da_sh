import NavBarComponent from "./index";
import "../../../__mocks__/watchMediaMock";

jest.mock("@utils/template", () => ({
  renderTemplate: jest.fn(
    () =>
      '<div js-attr="navbar-component" class="navbar-section"><div class="navbar-section__menu menu"><ul class="menu__links link-group"><li js-attr="link" js-path="about" class=" link-group__link link ">About</li><li js-attr="link" js-path="settings" class=" link-group__link link">Settings</li><li js-attr="link" js-path="Option1" class=" link-group__link link">Option1</li><li js-attr="link" js-path="Option2" class=" link-group__link link"> Option2</li><li js-attr="link" js-path="Option2" class=" link-group__link link">Option3</li></ul></div></div>'
  )
}));

jest.mock("@pages/Home", () => ({
  goTo: jest.fn()
}));
jest.mock("@components/Followers", () => ({
  render: jest.fn(),
  mounted: jest.fn()
}));

test("testing NavBar", () => {
  // expect(true).toBe(true);
  const template =
    '<div js-attr="navbar-component" class="navbar-section"><div class="navbar-section__menu menu"><ul class="menu__links link-group"><li js-attr="link" js-path="about" class=" link-group__link link ">About</li><li js-attr="link" js-path="settings" class=" link-group__link link">Settings</li><li js-attr="link" js-path="Option1" class=" link-group__link link">Option1</li><li js-attr="link" js-path="Option2" class=" link-group__link link"> Option2</li><li js-attr="link" js-path="Option2" class=" link-group__link link">Option3</li></ul></div></div>';
  document.body.innerHTML = template;
  // NavBarComponent.render();
  NavBarComponent.mounted();
  // window.resizeTo(950, 950);
  // window.resizeTo(0, 0);
  // window.innerWidth = 950;
  // window.dispatchEvent(new Event("change"));
  // document.querySelectorAll('[js-attr="link"]')[0].click();
  expect(true).toBe(true);
});
