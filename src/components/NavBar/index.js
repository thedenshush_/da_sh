import { renderTemplate } from "@utils/template";
import template from "./index.html";
import "./styles.scss";
import Home from "@pages/Home";
import FollowersComponent from "@components/Followers";
export class NavBarComponent {
  constructor() {
    // this.FollowersComponent = new FollowersComponent();
    this.state = {
      name: "navbar-component",
      elementName: "navbar-component",
      titleAbout: "ABOUT",
      titleSettings: "SETTINGS",
      titleOptionOne: "OPTION1",
      titleOptionTwo: "OPTION2",
      titleOptionThree: "OPTION3",
      /* istanbul ignore next */
      FollowersComponent: FollowersComponent.render()
    };
    this.elementName = this.state.elementName;
    this.node = null;
  }

  mounted() {
    this.node = document.querySelector(`[js-attr="${this.elementName}"]`);
    FollowersComponent.mounted();
    const mobileSize = window.matchMedia("(max-width: 940px)");
    mobileSize.addEventListener("change", media => {
      if (media.matches) {
        this.node
          .querySelector(`[js-attr="${FollowersComponent.state.elementName}"]`)
          .classList.add("navbar-section__followers-hide");
      } else {
        this.node
          .querySelector(`[js-attr="${FollowersComponent.state.elementName}"]`)
          .classList.remove("navbar-section__followers-hide");
      }
    });

    this.node.querySelectorAll('[js-attr="link"]').forEach(link => {
      link.addEventListener("click", () => {
        link.classList.add("link-active");
        //Change route

        Home.goTo(link.getAttribute("js-path"));
        history.pushState({}, "", `${link.getAttribute("js-path")}`);
      });
    });
  }
  events = {
    click: () => {}
  };
  render() {
    return renderTemplate(template, this.state);
  }
}

const navBarComponent = new NavBarComponent();
export default navBarComponent;
