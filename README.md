# Code Challenge

## Feedback

First of all, thank you for the challenge for reasons of time I didn't finish all testing, I test like half of the project.

### Bad things: the is several bugs that are not completed in the page

1. Navbar does not highlight the active section, that happens because if route change, I re-render the page.
2. inputs are not inserting values, in code challenge it won't I know how to do it, you can add the event listener to input and just change the value., however, I don't know why is not working there. I think is because I re-render the HTML when modal is closing.
3. Swipe is working only from about to settings, not returned, It happens because I cant re-render child dynamically with the parent node :/ (It gonna take too much time re-writing the template manager)4) I didn't include options tabs, just because of it the same thing.
4. I use flexbox for grid system, Honestly, I tried to adapt it to old browsers, in code you can see some "web-kit-flex" args. However I don't know why it is not working, I didn't have too much experience writing UI kit to support old browsers. Usually, I already had a UI kit. Sorry, about that. I gonna google it and practice more. At least I tried...6) the about page is not responsive to mobile(a question of time because I know how to do it)
5. I'm not good at writing UI from scratch and I know it, I should practice more.
6. I used bem in project, however I'm not sure if all project has bem

### Positive things:

1. I write some kind of micro react library with components with lifecycle hooks (just two hehe) so you can see that this code is more or less beautiful. I didn't want just import files, I wanted to make something bigger than just files, I split Html, js, and sass into different files, and load them using webpack. I think it's awesome.

2. I write some kind of dynamic router. It's awesome too, however that router gave me lots of bugs. It's a big shame and I'm sorry. but I tried, this is my first router in vanilla.

3. I write a small template manager. you said, that I should not hardcore the text, so I decided to write an Html template manager. It only accepts variables, I didn't have time to enrich it with more functionality.
4. I wrote some kind of webpack setup for dev and production.
5. I wirte tests using test coverage, I like to se the coverage of testing.

## Setup

```
yarn
yarn start //start webpack-dev-server
yarn dev // build dev bundle
yarn build // build prod bundle
yarn dev-server // build dev bundle and run local server
yarn stats // webpack bundle stats
yarn test // jest test + coverage
```

## FOR RUN TESTING !!!

Honestly I don't know why(I spent the last night trying to solve it) if you just run test it won't work.
For run the testing you should comment `/src/index.js` both imports, after that you can run `yarn test`

# Thank you for the challenge
