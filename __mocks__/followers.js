export const renderMock = jest.fn();
const mock = jest.fn().mockImplementation(() => {
  return { render: renderMock };
});

export default mock;
